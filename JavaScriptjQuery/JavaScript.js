﻿window.onload = function () {
    setElementValue('step', '1');
}
function showErrorMessage(error_message) {
    console.log(error_message);
    getById('error_message').innerHTML = error_message;
}

function clearErrorMessage() {
    getById('error_message').innerHTML = '';
}

//#1 Step

function checkLogin(login) {
    //Логин пользователя должен содержать минимум одну букву в верхнем регистре, должен заканчиваться на
    //цифру, длина должна быть не меньше 5 символов, состоять из английских букв и цифр
    var str = login;
    if (str.match(/^(?=.*[A-Z]).{4,}\d$/))
        return true;
    else {
        showErrorMessage('' +
            '<p>' +
            'Логин пользователя должен содержать минимум одну букву в верхнем регистре, ' + '<br>' +
            'должен заканчиваться на цифру, длина должна быть не меньше 5 символов, ' + '<br>' +
            'состоять из английских букв и цифр!' + '<br></p>');
        return false;
    }

}
function checkPassword(password) {
    //В пароле должны присутствовать символы трех категорий из числа следующих четырех:
    //прописные буквы английского алфавита от A до Z;
    //строчные буквы английского алфавита от a до z;
    //десятичные цифры (от 0 до 9);
    //неалфавитные символы (например, !, $, #, %)
    var str = password;
    var countGroups = 0;
    if (str.match(/[a-z]+/))
        countGroups++;
    if (str.match(/[A-Z]+/))
        countGroups++;
    if (str.match(/[0-9]+/))
        countGroups++;
    if (str.match(/[-_!@#$%*\(\)]+/))
        countGroups++;
    if (countGroups >= 3)
        return true;
    else {
        showErrorMessage('' +
            '<p>' +
            'В пароле должны присутствовать символы трех категорий из числа следующих четырех: ' + '<br>' +
            '- прописные буквы английского алфавита от A до Z;, ' + '<br>' +
            '- строчные буквы английского алфавита от a до z;, ' + '<br>' +
            '- десятичные цифры (от 0 до 9);, ' + '<br>' +
            '- неалфавитные символы (например, !, $, #, %)!' + '<br></p>');
        return false;
    }
}

function checkPasswordRepeat(password, password_repeat) {
    if (password.localeCompare(password_repeat) == 0) {
        return true;
    }
    else {
        showErrorMessage('Подтверждения пароля и пароль должны совпадать!');
        return false;
    }
}

//#2 Step

function checkName(name) {
    //Поле ФИО может содержать только символы и состоять из 3 слов, разделенных пробелом
    if (name.match(/^(?!.*[-_!@#$%*\(\)])(\D+\s){2}\D+$/)) {
        return true;
    }
    else {
        showErrorMessage('Поле ФИО может содержать только символы и состоять из 3 слов, разделенных пробелом!');
        return false;
    }
}

function checkBirthday(birthday) {
    //Дата рождения должна быть валидной датой в формате dd/mm/yyyy
    if (birthday.match(/^[0-3][0-9]\/(0|1)[0-9]\/[0-9]{4}$/)) {
        // remove any leading zeros from date values
        birthday = birthday.replace(/0*(\d*)/gi, "$1");
        var dateArray = birthday.split(/[\.|\/|-]/);

        // correct month value
        dateArray[1] = dateArray[1] - 1;

        // correct year value
        if (dateArray[2].length < 4) {
            // correct year value
            dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
        }

        var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
        if (testDate.getDate() != dateArray[0] || testDate.getMonth() != dateArray[1] || testDate.getFullYear() != dateArray[2]) {
            showErrorMessage('Дата рождения должна быть валидной датой в формате dd/mm/yyyy!');
            return false;
        } else {
            var now = new Date();
            if (now < testDate) {
                showErrorMessage('Дата рождения не может быть датой в будущем!');
                return false;
            }
            else
                return testDate;
        }
    } else {
        showErrorMessage('Дата рождения должна быть валидной датой в формате dd/mm/yyyy!');
        return false;
    }
}

function calcAge(birthDate) {
    todayDate = new Date();
    todayYear = todayDate.getFullYear();
    todayMonth = todayDate.getMonth();
    todayDay = todayDate.getDate();

    birthYear = birthDate.getFullYear();
    birthMonth = birthDate.getMonth();
    birthDay = birthDate.getDate();

    var age = todayYear - birthYear;

    if (todayMonth < birthMonth - 1) {
        age--;
    }

    if (birthMonth - 1 == todayMonth && todayDay < birthDay) {
        age--;
    }
    return age;
}

function checkAge(age) {
    if (age < 12) {
        showErrorMessage('Регистрация только с 12!');
        return false;
    }
    else
        return true;
}

//#3 Step

function checkZip(zip) {
    //Почтовый индекс должен содержать только 5 цифр
    if (zip.match(/^\d{5}$/))
        return true;
    else {
        showErrorMessage('Почтовый индекс должен содержать только 5 цифр!');
        return false;
    }
}

//#4 Step

function checkEmail(email) {
    if (email.match(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/))
        return true;
    else {
        showErrorMessage('Email должен содержать валидный адрес электронной почты!');
        return false;
    }


    return true;
}

function checkSubscription(className) {
    var els = document.getElementsByClassName(className);
    var checked = false;
    [].forEach.call(els, function (el) {
        if (el.checked) checked = true;
    });
    if (!checked)
        showErrorMessage('Вы должны выбрать хотя бы одну рассылку!');
    return checked;
}


function checkData(step) {

    clearErrorMessage();
    step = parseInt(step);

    switch (step) {
        case 1:
            var login = getElementValue('login');
            var password = getElementValue('password');
            var password_repeat = getElementValue('password_repeat');
            if (checkLogin(login) && checkPassword(password) && checkPasswordRepeat(password, password_repeat))
                return true;
            break;
        case 2:
            var name = getElementValue('name');
            var birthday = getElementValue('birthday');

            if (checkName(name)) {

                //Если дата рождения валидна, посчитать текущий возраст пользователя и записать в лейбл.
                if (validDate = checkBirthday(birthday)) {
                    var age = calcAge(validDate);
                    setElementHTML('age', age);
                    if (checkAge(age)) {
                        //записать в лейбл (решено записать в любом случае, чтобы видеть возраст даже при ошибке)
                        return true;
                    }
                    else {
                        return false;
                    }
                }

            }
            else return false;
            break;
        case 3:
            var zip = getElementValue('zip');
            if (checkZip(zip))
                return true;
            else return false;
            break;
        case 4:
            var email = getElementValue('email');
            var wantSubscription = isChecked('receivenews');
            if (!checkEmail(email)) return false;
            else if (wantSubscription) {
                if (!checkSubscription('subscriptions'))
                    return false;
            }
            return true;
            break;
        case 5:
            return true;
            break;
        default:
            ;
    }
    return false;
}


function goBack() {
    var step = getById('step').value;
    step--;
    getById('step').value = step;
    changeStep(step);
}

function goNext() {
    var step = getById('step').value;
    if (checkData(step)) {
        step++;
        getById('step').value = step;
        changeStep(step);
    }
    else {
    }
}

function finish() {
    if (checkData(1) && checkData(2) && checkData(3) && checkData(4)) {
        deactivateAllTabsAndHideAllFields();
        show('finished');
        disable('backbtn');
        disable('nextbtn');
        disable('finishbtn');

        alert('Регистрация прошла успешно');
    }
}


function getById(id) {
    return document.getElementById(id);
}

function getElementValue(id) {
    return getById(id).value;
}

function setElementValue(id, value) {
    getById(id).value = value;
}

function isChecked(id) {
    return getById(id).checked;
}

function getElementHTML(id) {
    return getById(id).innerHTML;
}

function setElementHTML(id, value) {
    getById(id).innerHTML = value;
}

function show(id) {
    getById(id).style.display = 'inline';
}

function hide(id) {
    getById(id).style.display = 'none';
}

function disable(id) {
    getById(id).disabled = true;
}

function enable(id) {
    getById(id).disabled = false;
}

function activate(id) {
    getById(id).className = "active";
}

function deactivate(id) {
    getById(id).className = "";
}

function deactivateAllTabsAndHideAllFields() {
    for (i = 1; i <= 5; i++) {
        deactivate('tab' + i);
        hide('fields' + i);
    }
}


function changeStep(step) {

    deactivateAllTabsAndHideAllFields();

    switch (step) {
        case 1:
            activate('tab1');
            disable('backbtn');
            show('fields1');
            break;
        case 2:
            activate('tab2');
            enable('backbtn');
            show('fields2');
            break;
        case 3:
            activate('tab3');
            show('fields3');
            break;
        case 4:
            activate('tab4');
            show('fields4');
            enable('nextbtn');
            break;
        case 5:
            setElementHTML('loginspan', getElementValue('login'));
            setElementHTML('namespan', getElementValue('name'));
            setElementHTML('birthdayspan', getElementValue('birthday'));
            setElementHTML('agespan', getElementHTML('age'));
            setElementHTML('addressspan', getElementValue('address'));
            setElementHTML('cityspan', getElementValue('city'));
            setElementHTML('regionspan', getElementValue('region'));
            setElementHTML('zipspan', getElementValue('zip'));
            setElementHTML('emailspan', getElementValue('email'));
            if (isChecked('receivenews')) {
                show('subscriptionslabel');
                setElementHTML('receivenewsspan', 'Да');
                var els = document.getElementsByClassName('subscriptions');
                var lists = '';
                [].forEach.call(els, function (el) {
                    if (el.checked) {
                        if (lists != '') lists += ', ';
                        lists += el.value;
                    }
                });
                setElementHTML('subscriptionsspan', lists);
            }
            else {
                hide('subscriptionslabel');
                setElementHTML('receivenewsspan', 'Нет');
            }

            activate('tab5');
            show('fields5');
            show('finishbtn');
            disable('nextbtn');
            break;

        default:
    }

}