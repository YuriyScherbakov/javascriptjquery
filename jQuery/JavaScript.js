﻿$(document).ready(function () {
    $('#step').val('1');

    var hiddenBox = $("#banner-message");
    $("#backbtn").on("click", function (event) {
        goBack();
        return false;
    });

    $("#nextbtn").on("click", function (event) {
        goNext();
        return false;
    });

    $("#finishbtn").on("click", function (event) {
        finish();
        return false;
    });

    $("#receivenews").on("click", function (event) {
        $("#subscription").toggle(this.checked);
    });

    $("form input").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('#nextbtn').click();
            return false;
        } else {
            return true;
        }
    });
});

function showErrorMessage(error_message) {
    $('#error_message').html(error_message);
}

function clearErrorMessage() {
    $('#error_message').html('');
}

function checkSubscription(className) {
    var els = $("." + className);
    var checked = false;
    [].forEach.call(els, function (el) {
        if (el.checked) checked = true;
    });
    if (!checked)
        showErrorMessage('Вы должны выбрать хотя бы одну рассылку!');
    return checked;
}


function checkData(step) {

    clearErrorMessage();
    step = parseInt(step);

    switch (step) {
        case 1:
            var login = $('#login').val();
            var password = $('#password').val();
            var password_repeat = $('#password_repeat').val();
            if (checkLogin(login) && checkPassword(password) && checkPasswordRepeat(password, password_repeat))
                return true;
            break;
        case 2:
            var name = $('#name').val();
            var birthday = $('#birthday').val();

            if (checkName(name)) {

                //Если дата рождения валидна, посчитать текущий возраст пользователя и записать в лейбл.
                if (validDate = checkBirthday(birthday)) {
                    var age = calcAge(validDate);
                    $('#age').html(age);
                    if (checkAge(age)) {
                        //записать в лейбл (решено записать в любом случае, чтобы видеть возраст даже при ошибке)
                        return true;
                    }
                    else {
                        return false;
                    }
                }

            }
            else return false;
            break;
        case 3:
            var zip = $('#zip').val();
            if (checkZip(zip))
                return true;
            else return false;
            break;
        case 4:
            var email = $('#email').val();
            var wantSubscription = $("#receivenews").is(':checked');
            if (!checkEmail(email)) return false;
            else if (wantSubscription) {
                if (!checkSubscription('subscriptions'))
                    return false;
            }
            return true;
            break;
        case 5:
            return true;
            break;
        default:
            ;
    }
    return false;
}


function goBack() {
    var step = $('#step').val();
    step--;
    $('#step').val(step);
    changeStep(step);
}

function goNext() {
    var step = $('#step').val();
    if (checkData(step)) {
        step++;
        $('#step').val(step);
        changeStep(step);
    }
    else {
    }
}

function finish() {
    if (checkData(1) && checkData(2) && checkData(3) && checkData(4)) {
        deactivateAllTabsAndHideAllFields();
        $('#finished').show();
        $('#backbtn').prop('disabled', true);
        $('#nextbtn').prop('disabled', true);
        $('#finishbtn').prop('disabled', true);

        alert('Регистрация прошла успешно');
    }
}

function activate(id) {
    $("id").toggleClass("active");
}


function deactivate(id) {
    $("id").toggleClass("");
}

function deactivateAllTabsAndHideAllFields() {
    for (i = 1; i <= 5; i++) {
        deactivate('tab' + i);
        $('#fields' + i).hide();
    }
}

//#1 Step

function checkLogin(login) {
    //Логин пользователя должен содержать минимум одну букву в верхнем регистре, должен заканчиваться на
    //цифру, длина должна быть не меньше 5 символов, состоять из английских букв и цифр
    var str = login;
    if (str.match(/^(?=.*[A-Z]).{4,}\d$/))
        return true;
    else {
        showErrorMessage('' +
            '<p>' +
            'Логин пользователя должен содержать минимум одну букву в верхнем регистре, ' + '<br>' +
            'должен заканчиваться на цифру, длина должна быть не меньше 5 символов, ' + '<br>' +
            'состоять из английских букв и цифр!' + '<br></p>');
        return false;
    }

}
function checkPassword(password) {
    //В пароле должны присутствовать символы трех категорий из числа следующих четырех:
    //прописные буквы английского алфавита от A до Z;
    //строчные буквы английского алфавита от a до z;
    //десятичные цифры (от 0 до 9);
    //неалфавитные символы (например, !, $, #, %)
    var str = password;
    var countGroups = 0;
    if (str.match(/[a-z]+/))
        countGroups++;
    if (str.match(/[A-Z]+/))
        countGroups++;
    if (str.match(/[0-9]+/))
        countGroups++;
    if (str.match(/[-_!@#$%*\(\)]+/))
        countGroups++;
    if (countGroups >= 3)
        return true;
    else {
        showErrorMessage('' +
            '<p>' +
            'В пароле должны присутствовать символы трех категорий из числа следующих четырех: ' + '<br>' +
            '- прописные буквы английского алфавита от A до Z;, ' + '<br>' +
            '- строчные буквы английского алфавита от a до z;, ' + '<br>' +
            '- десятичные цифры (от 0 до 9);, ' + '<br>' +
            '- неалфавитные символы (например, !, $, #, %)!' + '<br></p>');
        return false;
    }
}

function checkPasswordRepeat(password, password_repeat) {
    if (password.localeCompare(password_repeat) == 0) {
        return true;
    }
    else {
        showErrorMessage('Подтверждения пароля и пароль должны совпадать!');
        return false;
    }
}

//#2 Step

function checkName(name) {
    //Поле ФИО может содержать только символы и состоять из 3 слов, разделенных пробелом
    if (name.match(/^(?!.*[-_!@#$%*\(\)])(\D+\s){2}\D+$/)) {
        return true;
    }
    else {
        showErrorMessage('Поле ФИО может содержать только символы и состоять из 3 слов, разделенных пробелом!');
        return false;
    }
}

function checkBirthday(birthday) {
    //Дата рождения должна быть валидной датой в формате dd/mm/yyyy
    if (birthday.match(/^[0-3][0-9]\/(0|1)[0-9]\/[0-9]{4}$/)) {
        // remove any leading zeros from date values
        birthday = birthday.replace(/0*(\d*)/gi, "$1");
        var dateArray = birthday.split(/[\.|\/|-]/);

        // correct month value
        dateArray[1] = dateArray[1] - 1;

        // correct year value
        if (dateArray[2].length < 4) {
            // correct year value
            dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
        }

        var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
        if (testDate.getDate() != dateArray[0] || testDate.getMonth() != dateArray[1] || testDate.getFullYear() != dateArray[2]) {
            showErrorMessage('Дата рождения должна быть валидной датой в формате dd/mm/yyyy!');
            return false;
        } else {
            var now = new Date();
            if (now < testDate) {
                showErrorMessage('Дата рождения не может быть датой в будущем!');
                return false;
            }
            else
                return testDate;
        }
    } else {
        showErrorMessage('Дата рождения должна быть валидной датой в формате dd/mm/yyyy!');
        return false;
    }
}

function calcAge(birthDate) {
    todayDate = new Date();
    todayYear = todayDate.getFullYear();
    todayMonth = todayDate.getMonth();
    todayDay = todayDate.getDate();

    birthYear = birthDate.getFullYear();
    birthMonth = birthDate.getMonth();
    birthDay = birthDate.getDate();

    var age = todayYear - birthYear;

    if (todayMonth < birthMonth - 1) {
        age--;
    }

    if (birthMonth - 1 == todayMonth && todayDay < birthDay) {
        age--;
    }
    return age;
}

function checkAge(age) {
    if (age < 12) {
        showErrorMessage('Регистрация только с 12!');
        return false;
    }
    else
        return true;
}

//#3 Step

function checkZip(zip) {
    //Почтовый индекс должен содержать только 5 цифр
    if (zip.match(/^\d{5}$/))
        return true;
    else {
        showErrorMessage('Почтовый индекс должен содержать только 5 цифр!');
        return false;
    }
}

//#4 Step

function checkEmail(email) {
    if (email.match(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/))
        return true;
    else {
        showErrorMessage('Email должен содержать валидный адрес электронной почты!');
        return false;
    }


    return true;
}


function changeStep(step) {

    deactivateAllTabsAndHideAllFields();

    switch (step) {
        case 1:
            activate('tab1');
            $('#backbtn').prop('disabled', true);
            $('#fields1').show();
            break;
        case 2:
            activate('tab2');
            $('#backbtn').prop('disabled', false);
            $('#fields2').show();
            break;
        case 3:
            activate('tab3');
            $('#fields3').show();
            break;
        case 4:
            activate('tab4');
            $('#fields4').show();
            $('#nextbtn').prop('disabled', false);
            break;
        case 5:
            $('#error_message').html(error_message);
            $('#loginspan').html($('#login').val());
            $('#namespan').html($('#name').val());
            $('#birthdayspan').html($('#birthday').val());
            $('#agespan', $('#age').html());
            $('#addressspan').html($('#address').val());
            $('#cityspan').html($('#city').val());
            $('#regionspan').html($('#region').val());
            $('#zipspan').html($('#zip').val());
            $('#emailspan').html($('#email').val());
            if ($("#receivenews").is(':checked')) {
                $("#subscriptionslabel").show();
                $('#receivenewsspan').html('Да');
                var els = $(".subscriptions");
                var lists = '';
                [].forEach.call(els, function (el) {
                    if (el.checked) {
                        if (lists != '') lists += ', ';
                        lists += el.value;
                    }
                });
                $('#subscriptionsspan').html(lists);
            }
            else {
                $('#subscriptionslabel').hide();
                $('#receivenewsspan').html('Нет');
            }

            activate('tab5');
            $('#fields5').show();
            $('#finishbtn').show();
            $('#nextbtn').prop('disabled', true);
            break;

        default:
    }



}