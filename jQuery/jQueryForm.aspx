﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jQueryForm.aspx.cs" Inherits="jQuery.jQueryForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>JS Form (jQuery)</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script type="text/javascript" src="jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="JavaScript.js"></script>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <form id="form1" runat="server">
   
<div class="container">

    <ul>
        <li class="active" id="tab1">Данные Аутентификации</li>
        <li id="tab2">Личная информация</li>
        <li id="tab3">Адрес</li>
        <li id="tab4">Подписка на уведомления</li>
        <li id="tab5">Проверка данных</li>
    </ul>

    <form id="form">

        <div id="error_message" style="color: red;"></div>

        <div id="fields1">

            <div class="form-group">
                <label for="login">Логин пользователя:</label>
                <input type="text" class="form-control" id="login">
            </div>
            <div class="form-group">
                <label for="password">Пароль:</label>
                <input type="password" class="form-control" id="password">
            </div>
            <div class="form-group">
                <label for="password_repeat">Подтверждения пароля:</label>
                <input type="password" class="form-control" id="password_repeat">
            </div>
        </div>

        <div id="fields2" style="display: none;">

            <div class="form-group">
                <label for="name">ФИО:</label>
                <input type="text" class="form-control" id="name">
            </div>
            <div class="form-group">
                <label for="birthday">Дата рождения:</label>
                <input type="text" class="form-control" id="birthday" placeholder="dd/mm/yyyy">
            </div>

            <div class="form-group">
                <label for="age">Возраст:</label>
                <label id="age">?</label>
            </div>

        </div>


        <div id="fields3" style="display: none;">

            <div class="form-group">
                <label for="address">Адрес:</label>
                <input type="address" class="form-control" id="address">
            </div>
            <div class="form-group">
                <label for="city">Город:</label>
                <input type="text" class="form-control" id="city" value="Харьков">
            </div>

            <div class="form-group">
                <label for="region">Область:</label>
                <select name="region" class="form-control" id="region">
                    <option value="Винницкая">Винницкая область</option>
                    <option value="Волынская">Волынская область</option>
                    <option value="Днепропетровская">Днепропетровская область</option>
                    <option value="Донецкая">Донецкая область</option>
                    <option value="Житомирская">Житомирская область</option>
                    <option value="Закарпатская">Закарпатская область</option>
                    <option value="Запорожская">Запорожская область</option>
                    <option value="Ивано-Франковская">Ивано-Франковская область</option>
                    <option value="Киевская">Киевская область</option>
                    <option value="Кировоградская">Кировоградская область</option>
                    <option value="Луганская">Луганская область</option>
                    <option value="Львовская">Львовская область</option>
                    <option value="Николаевская">Николаевская область</option>
                    <option value="Одесская">Одесская область</option>
                    <option value="Полтавская">Полтавская область</option>
                    <option value="Ровненская">Ровненская область</option>
                    <option value="Сумская">Сумская область</option>
                    <option value="Тернопольская">Тернопольская область</option>
                    <option value="Харьковская" selected="selected">Харьковская область</option>
                    <option value="Херсонская">Херсонская область</option>
                    <option value="Хмельницкая">Хмельницкая область</option>
                    <option value="Черкасская">Черкасская область</option>
                    <option value="Черниговская">Черниговская область</option>
                    <option value="Черновицкая">Черновицкая область</option>
                    <option value="Автономная Республика Крым">Автономная Республика Крым</option>
                </select>
            </div>

            <div class="form-group">
                <label for="zip">Почтовый индекс:</label>
                <input type="text" class="form-control" id="zip" maxlength="5">
            </div>
        </div>


        <div id="fields4" style="display: none;">

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" id="email" placeholder="Enter email">
            </div>
            <div class="form-group">
                <input type="checkbox" id="receivenews"> Хотите ли вы получать дополнительные новости<br>
            </div>

            <div class="form-group" id="subscription" style="display: none;">
                <label>Список подписок</label><br>
                <input class="subscriptions" type="checkbox" name="subscription" value="Политика"> Политика<br>
                <input class="subscriptions" type="checkbox" name="subscription" value="Спорт"> Спорт<br>
                <input class="subscriptions" type="checkbox" name="subscription" value="Музыка"> Музыка<br>
            </div>

        </div>


        <div id="fields5" style="display: none;">

            <div class="form-group">
                <p><label>Логин пользователя:</label> <span id="loginspan"></span></p>
                <p><label>Пароль:</label> <span id="passwordspan">***скрыт***</span></p>
                <p><label>ФИО:</label> <span id="namespan"></span></p>
                <p><label>Дата рождения:</label> <span id="birthdayspan"></span></p>
                <p><label>Возраст:</label> <span id="agespan"></span></p>
                <p><label>Адрес:</label> <span id="addressspan"></span></p>
                <p><label>Город:</label> <span id="cityspan"></span></p>
                <p><label>Область:</label> <span id="regionspan"></span></p>
                <p><label>Почтовый индекс:</label> <span id="zipspan"></span></p>
                <p><label>Email:</label> <span id="emailspan"></span></p>
                <p><label>Хотите ли вы получать дополнительные новости:</label> <span id="receivenewsspan"></span></p>
                <p><label id="subscriptionslabel" style="display: none;">Список подписок:</label> <span
                        id="subscriptionsspan"></span></p>
            </div>

        </div>


        <div id="finished" style="display: none;">

            <div class="form-group">
                Регистрация прошла успешно!
            </div>

        </div>

        <div id="buttons">

            <input type="button" id="backbtn" disabled class="btn btn-default"
                   value="Назад">
            <input type="button" id="nextbtn" class="btn btn-default"
                   value="Вперед">
            <input style="display: none;" type="button" id="finishbtn" class="btn btn-default"
                   value="Завершить">
            <input type="hidden" id="step" value="1">

        </div>
    </form>
</div>

    </form>
</body>
</html>
